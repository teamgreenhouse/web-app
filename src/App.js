/*
 * Name : AddUser.js
 * Author : Katpally Sravya
 * Description : This is the app main page, where the execution starts from here
 * Date of initiation : 04/02/2020
 * */

import React,{Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {NavLink} from 'react-router-dom';
//import logo from './logo.svg';
import imgLD from './lettucedream.jpg';
import './App.css';
import './index.css'
//import { domainToASCII } from 'url';
import AddUser from './pages/AddUser';
import AddDetails from './pages/AddDetails';
import AddCustomer from './pages/AddCustomer';
import AddBay from './pages/AddBay';
import AddPackage from './pages/AddPackage';
import ReadBarcode from './pages/ReadBarcode';
import GenerateBarcode from './pages/GenerateBarcode';
import UpdateUser from './pages/UpdateUser';
import MainPage from './pages/MainPage';
import Login from './pages/Login';

class App extends Component {
  render()
  {
    return (
      <Router basename= "/react-auth-ui/">
        <div className="App"> 

        {/* //Header navigation bar */}
          <nav className="navbar navbar-expand-md fixed-top" >
            
            <img src= {imgLD} width ="8%" height ="100%" alt ="jjkkkk"/><br></br>
            
          </nav><br></br><br></br><br></br>

              {/*Giving url to each component which redirects to their respective components  */}
          <Route exact path="/login" component={Login} />
          <Route exact path="/main" component={MainPage} />
          <Route exact path="/add-user" component={AddUser} />
          <Route exact path="/add-details" component={AddDetails} />
          <Route exact path="/add-customer" component={AddCustomer} />
          <Route exact path="/add-bay" component={AddBay} />
          <Route exact path="/add-package" component={AddPackage} />
          <Route exact path="/add-barcode" component={ReadBarcode} />
          {/* <Route exact path="/add-generatebarcode" component={GenerateBarcode} /> */}
          <Route exact path="/update-user" component={UpdateUser}/>
          


              {/* // Footer naving bar */}
          <nav className="navbar navbar-expand-md fixed-bottom" >
            Copy Right,
          </nav>
        </div>  
      </Router>    
    );
  }
  }
  

export default App;
