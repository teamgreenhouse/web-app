/*
 * Name : AddUser.js
 * Author : Katpally Sravya
 * Description : This code helps admin to add users
 * Date of initiation : 02/15/2020
 * */

import React, { Component } from 'react';
import axios from 'axios';
import ApiCall from './ApiCall'

class AddUser extends Component {
	constructor(props) {
        super(props)

        // declaring all the variables initially to the state
        this.state = 
        {
            fields: {},
            errors: {},
			firstName: '',
			lastName: '',
            dateOfBirth: '',
            accessLevel:'',
            phoneNumber: '',
            password: '',
            streetAddress: '',
            city: '',			
            state: '',
            zipCode: '',
        }
        this.submitForm = this.submitForm.bind(this);   
    }

    // This method handles the validation for all the fields in the form 
    // If the form value is empty, it shows a messages as "Cannot be empty"
    handleValidation()
    {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
 
        if(!this.state.lastName )
        {
            formIsValid = false;
            errors["lastName"] = "Cannot be empty";               
        }
        else if(!this.state.firstName)
        {
            formIsValid = false;
            errors["firstName"]= "Cannot be empty";
        }
        else if(!this.state.state)
        {
            formIsValid = false;
            errors["state"]= "Cannot be empty";
        }
        else if(!this.state.dateOfBirth)   
        {
            formIsValid = false;
            errors["dateOfBirth"]= "Cannot be empty";   
        }
        else if(!this.state.password )
        {
            formIsValid = false;
            errors["password"]= "Cannot be empty";
        }
        else if(!this.state.city)
        {
            formIsValid = false;
            errors["city"]= "Cannot be empty";        
        }
        else if(!this.state.phoneNumber)
        {
            formIsValid = false;
            errors["phoneNumber"]= "Cannot be empty";
        }
        else if(!this.state.zipCode)
        {
            formIsValid = false;
            errors["zipCode"]= "Cannot be empty";
        }
        else if(!this.state.streetAddress)
        {
            formIsValid = false;
            errors["streetAddress"]= "Cannot be empty";   
        }

        else if((this.state.accessLevel) === "")
        {
            formIsValid = false;
            errors["accessLevel"]= "Cannot be empty";   
        }
        this.setState({errors: errors});
        return formIsValid;
    }

    // This method checks the handleValidatioN method, if the fields are not empty, then it shows the alert message as
    // "Form Submitted" else "Form has errors"
    submitForm(e)
    {
        e.preventDefault();
 
        if(this.handleValidation())
        {
            alert("Form submitted")
        }
        else
        {
            alert("Form has errors.")
        }

        console.log(this.state)
    }

    // This method is for setting the values entered or changed in the fields to their respective state values
    // handleChange(field, e)
    // {         
    //     let fields = this.state.fields;
    //     fields[field] = e.target.value;        
    //     this.setState({fields});
    // }

    // In this method, we are setting the values changed in the json format to send the data to the API
    changeHandler = e =>
    {
		this.setState({ [e.target.name]: e.target.value })
    }

    // Calling method in ApiCall class which is Api calling, I commented this code becoz
    //currently api call is not working bezoz it is localhost api which we didn't deploy into the server 
    // sendingDataToApi()
    // {
    //     ApiCall.addUserCall()
    //     .then(
    //         console.log("Data stored successfully") // here we send the state data
    //     )
    // }
    
    // Here, it renders the components to web page 
    // it displayes the fields written like firstName,lastName,dateOfBirth,accessLevel,phoneNumber,
                //password,streetAddress,city,state,zipCode and buttons
    // and on changes, calls the appropriate methods written.
	render() {
		const {firstName,lastName,dateOfBirth,accessLevel,phoneNumber,password,streetAddress,city,state,zipCode} = this.state
		return (
			<div>
                <form> 
				    <div className="container-fluid" >
					    <div className="row"> 
                            <div className="col-md-5"></div>
                            <div className="col-md-2" >
                                <div className="text-primary" >
                                <h3><label htmlFor="main"><b>add user</b></label></h3>
						        </div>
						    </div>  
						    <div className="col-md-5"></div>
					    </div>

        		        <div className="row">   
                            <div className="col-md-3"></div>
                            <div className="col-md-4">  
                                <div className="form-group">
                                    <label htmlFor="firstName"><b>first name : </b></label><br></br> 
                                    <input type="text" name ="firstName" onChange={this.changeHandler}  /><br></br> 
                                    <span style={{color: "red"}}>{this.state.errors["firstName"]}</span><br></br>
                        
                                    <label htmlFor="dateOfBirth"><b>date of birth :</b></label><br></br> 
                                    <input type="text" name ="dateOfBirth" ref ="dateOfBirth"onChange={this.changeHandler } /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["dateOfBirth"]}</span><br></br>

                                    <label htmlFor="streetAddress"><b>street address :</b></label><br></br> 
                                    <input type="text" name ="streetAddress" ref ="streetAddress" onChange={this.changeHandler}   /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["streetAddress"]}</span><br></br>

                                    <label htmlFor="state"><b>state :</b></label><br></br> 
                                    <input type="text" name ="state" ref ="state"  onChange={this.changeHandler}  /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["state"]}</span><br></br>

                                    <label htmlFor="accessLevel"><b>Access level :</b></label><br></br> 
                                    <select value ={accessLevel} name ="accessLevel" onChange={this.changeHandler} >
                                        <option value="Intern">Select Type Of User</option>
                                        <option value="Intern">Intern</option>
                                        <option value="Volunteer">Volunteer</option>
                                        <option value="Admin">Admin</option>
                                    </select><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["accessLevel"]}</span><br></br>
                                </div>
                	        </div>
                	        <div className="col-md-4">  
                                <div className="form-group">
                                    <label htmlFor="lastName"><b>last name :</b>   </label><br></br> 
                                    <input type="text" name ="lastName" ref ="lastName" onChange={this.changeHandler}  /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["lastName"]}</span><br></br>
                        
                                    <label htmlFor="password"><b>password :</b></label><br></br> 
                                    <input type="password" name ="password" ref ="password" onChange={this.changeHandler}  /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["password"]}</span><br></br>

                                    <label htmlFor="city"><b>city :</b></label><br></br>
                                    <input type="text" name ="city" ref ="city" onChange={this.changeHandler}/><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["city"]}</span><br></br>

                                    <label htmlFor="zipCode"><b>zip code :</b></label><br></br> 
                                    <input type="text" name ="zipCode" ref ="zipCode" onChange={this.changeHandler}  /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["zipCode"]}</span><br></br>

                                    <label htmlFor="phoneNumber"><b>phone number :</b></label><br></br>
                                    <input type="text" name ="phoneNumber" ref ="phoneNumber"  onChange={this.changeHandler} /><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["phoneNumber"]}</span><br></br>
                                </div>
                            </div>
                            <div className="col-md-2"></div>              
                        </div>                                                      
                        <div className="row">   
                            <div className="col-md-4"> </div> 
                            <div className="col-md-2">
                                <button type="button" className="btn btn-default"  onClick={this.submitForm} >Submit</button>
                            </div>
                            <div className="col-md-2">
                                <button type="reset"  className="btn btn-default" >Cancel</button>
                            </div>
                            <div className="col-md-4"></div>
                        
                        </div><br></br><br></br>
                        <div className = "row">
                            <div className="col-md-1"></div>
                            <div className="col-md-3">
                                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/main`)} >Back</button>
                            </div>
                        </div><br></br><br></br><br></br><br></br>
                    </div>
                </form>
			</div>
		)
	}
}
export default AddUser;
