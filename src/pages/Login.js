import React from 'react'



import './login.css';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 'userId': '', 'password': '' }
    }

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
        
    };

     onSubmit = (e) =>{
        this.setState({
            userId :'Charlie',
            password : 'Charlie'
        })
        if(this.state.userId == 'Charlie' && this.state.password == 'Charlie')
        {
            this.props.history.push(`/main`)
        }
        else{
            alert('error')
        }

    }

    // onSubmit = async (e) => {
    //     e.preventDefault();
    //     const { userId, password } = this.state;
    //     const data = await login(this.state);
    //     alert("Authenticated"+JSON.stringify(data));
    //   }
    render() {
        return (
            <div>
            <section className="login-block">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 login-sec"><br></br>
                            <h2 className="text-center">Login </h2><br></br>
                            <form className="login-form" onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1" className="text-uppercase">Employee Id</label>
                                    <input type="text" className="form-control" placeholder="Enter userId " name="userId"
                                        value={this.state.userId}
                                        onChange={this.handleChange} />

                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1" className="text-uppercase">Password</label>
                                    <input type="password" className="form-control" placeholder="Enter Password" name="password"
                                        value={this.state.password}
                                        onChange={this.handleChange} />
                                </div>


                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input type="checkbox" className="form-check-input" />
                                        <small>Remember Me</small>
                                    </label>
                                    <button type="submit" className="btn btn-login float-right">Submit</button>
                                </div>

                            </form>

                        </div>
                        <div className="col-md-8 banner-sec">
                            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                                <div className="carousel-inner" role="listbox">
                                    <div className="carousel-item active">
                                        <img className="d-block img-fluid" src="http://www.lettucedream.org/wp-content/uploads/2017/04/Group-2-1.jpg" alt="First slide" />
                                        <div className="carousel-caption d-none d-md-block">
                                            <div className="banner-text">
                                                <h2>Welcome to Lettuce Dream Management Portal</h2>
                                                <p>Lettuce Dream, a social enterprise engaged in hydroponic farming, exists to provide meaningful employment and training opportunities for persons with cognitive or developmental disabilities.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
        );
    }
}


export default Login