import React, { Component } from 'react';
import axios from 'axios';
class UpdateUser extends Component{

    constructor(props){
        super(props);
 
        this.state = {
            fields: {},
            errors: {},
            access: '',
			phn: '',
			password: '',
            address:'',
        }
        this.submitForm = this.submitForm.bind(this);
    }
        handleValidation()
        {
         //let fields = this.state.fields;
         let errors = {};
         let formIsValid = true;
 
        if(!this.state.password )
         {
             formIsValid = false;
             errors["password"]= "Cannot be empty";
         }
         else if(!this.state.phn)
         {
             formIsValid = false;
             errors["phn"]= "Cannot be empty";        
         }
         else if(!this.state.phn)
         {
             formIsValid = false;
             errors["phoneNumber"]= "Cannot be empty";
         }
         else if(!this.state.access)
         {
             formIsValid = false;
             errors["access"]= "Cannot be empty";   
         }

        this.setState({errors: errors});
        return formIsValid;
    }
    submitForm(e)
        {
         e.preventDefault();
 
         if(this.handleValidation())
         {
            alert("Form submitted")
         }else{
            alert("Form has errors.")
         }

         console.log(this.state)
     }

        changeHandler = e => {
            this.setState({ [e.target.name]: e.target.value })
        }
    render()
    {
        const {access,phn,password,address} = this.state
        return(
            <form>
            <div className = "container-fluid">
                <div className="row"> 
                    <div className="col-md-5"></div>
                    <div className="col-md-2" >
                    <div className="text-primary" >
                        <h3>
                        <label htmlFor="main"><b>update user</b></label>
                        </h3>
                    </div>
                    </div>
                    <div className="col-md-5"></div>
                </div><br></br>

                <div className="row">   
                    <div className="col-md-3"></div>
                    <div className="col-md-4">  
                        <div className="form-group">
                        <label htmlFor="address"><b>street address :</b></label><br></br> 
                        <input type="text" name ="address" value = {address} ref ="address" onChange={this.changeHandler}  /><br></br>
                        <span style={{color: "red"}}>{this.state.errors["address"]}</span><br></br>

                        <label htmlFor="access"><b>Access level :</b></label><br></br> 
                                    <select value ={access} name ="access" onChange={this.changeHandler} >
                                        <option value="Intern">Select Type Of User</option>
                                        <option value="Intern">Intern</option>
                                        <option value="Volunteer">Volunteer</option>
                                        <option value="Admin">Admin</option>
                                    </select><br></br>
                                    <span style={{color: "red"}}>{this.state.errors["access"]}</span><br></br>

                        </div>
                    </div>
                        <div className="col-md-4">
                        <label htmlFor="phn"><b>phone number :</b></label><br></br>
                        <input type="text" name ="phn" value = {phn} ref ="phn"  onChange={this.changeHandler} /><br></br>
                        <span style={{color: "red"}}>{this.state.errors["phn"]}</span><br></br>

                        <label htmlFor="password"><b>password :</b></label><br></br> 
                        <input type="password" name ="password" value = {password} ref ="password" onChange={this.changeHandler}/><br></br>
                        <span style={{color: "red"}}>{this.state.errors["password"]}</span><br></br>
                        </div>
                        <div className="col-md-2"></div>
                </div>
                <div className="row">  

                <div className="col-md-3"></div> 
                
               
                <div className="col-md-3">
                    <button type="button" className="btn btn-default"  onClick={this.submitForm} >Submit</button>
                </div>
                
                <div className="col-md-3">
                <button type="reset"  className="btn btn-default" >Cancel</button>
                </div>  
                <div className="col-md-3"></div>
            </div><br></br><br></br><br></br><br></br>
            </div><br></br>

            <div className = "row">
            <div className="col-md-1"></div>
            <div className="col-md-3">
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/main`)} >Back</button>
            </div>
                

            </div>
            </form>
        );
    }
}
export default UpdateUser;