import React, { Component } from 'react';

class AddPackage extends Component{

    constructor(props){
        super(props);
 
        this.state = {
            fields: {},
            errors: {}
        }
        this.submitForm = this.submitForm.bind(this);
     }
     
     handleValidation(){
         let fields = this.state.fields;
         let errors = {};
         let formIsValid = true;
 
         //Name
         if(!fields["lname"] || !fields["fname"] || !fields["country"] || !fields["dob"] || !fields["sclname"]
         || !fields["city"]|| !fields["code"]|| !fields["address"]   )
         {
            formIsValid = false;
            errors["lname"] = "Cannot be empty";
            errors["fname"]= "Cannot be empty";
            errors["state"]= "Cannot be empty";
            errors["dob"]= "Cannot be empty";
            errors["password"]= "Cannot be empty";
            errors["city"]= "Cannot be empty";
            errors["phn"]= "Cannot be empty";
            errors["code"]= "Cannot be empty";
            errors["address"]= "Cannot be empty";
         }
        this.setState({errors: errors});
        return formIsValid;
    }
 
    submitForm(e){
         e.preventDefault();
 
         if(this.handleValidation()){
            alert("Form submitted");
         }else{
            alert("Form has errors.")
         }
 
     }
 
     handleChange(field, e){         
         let fields = this.state.fields;
         fields[field] = e.target.value;        
         this.setState({fields});
     }
 
    render(){
        return ( 
        <form>         
        <div class="container-fluid" >
            
            <div class="row"> 
                <div class="col-md-5"></div>
                <div class="col-md-2" >
                <div class="text-primary" >
                    <label for="main"><b><h3>ADD PACKAGE</h3></b></label>      
                </div>
                </div>
                <div class="col-md-5"></div>
            </div>
            <br></br>
        <div class="row"> 
                <div class="col-md-3"></div>
                <div class="col-md-3    ">
                <label for="phn"><b>PACKAGE TYPE :</b></label><br></br>
                <input type="text" ref ="code"  onChange={this.handleChange.bind(this, "code")} value={this.state.fields["code"]} /><br></br>
                        <span style={{color: "red"}}>{this.state.errors["code"]}</span><br></br>
                </div>
                <div class="col-md-2" >
                <label for="code"><b>WEIGHT</b></label><br></br>
                    <input type="checkbox" id="weight" name="weight" value="weight"></input>
                 </div>                
                <div class="col-md-2">
                <label for="phn"><b>COUNT</b></label><br></br>
                        <input type="checkbox" id="count" name="count" value="count"></input>           
                </div>
                <div class="row">
                            <div class="col-md-2">
                          
                            </div>
                        </div>           
                    </div>
            </div>
        

        <div class="row"> 
                <div class="col-md-3"></div>
               
                <div class="col-md-3" >
                    <button type="button" class="btn btn-default" id="sumbit" value="Submit" onClick={this.submitForm} >SUBMIT</button>
                </div>                
               
            </div>

            <div className = "row">
            <div className="col-md-1"></div>
            <div className="col-md-3">
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/main`)} >Back to Main Page</button>
                </div>
            </div><br></br><br></br><br></br><br></br>
          
        </form> 

            
    
        );
    }
}

export default AddPackage;