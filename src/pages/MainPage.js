/*
 * Name : MainPage.js
 * Author : Katpally Sravya
 * Description : This is the main page, which redirects to their respective pages
 * Date of initiation : 04/01/2020
 * */
import React, { Component } from 'react';

class MainPage extends Component {
	constructor(props) {
        super(props)
    }

    backButton = (e) =>{

        this.props.history.push(`/Login`)

    }
    // Here on button click, it redirects to their respective pages
    render(){
        return(
            <div className = "container"> <br></br>
            <div>
                <h1 className = "welcome">Welcome Charlie!!!!!</h1><br></br><br></br><br></br><br></br>
                <button className="btn btn-default" onClick = {() => this.props.history.push(`/add-user`)} >Add User</button>
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/update-user`)}>Update User   </button>
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/add-details`)}>Add Details</button>
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/add-customer`)}>Add Customer</button>
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/add-bay`)}>Add Bay</button>
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/add-package`)}>Add Package</button>
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/add-barcode`)}>Generate Barcode list</button>
            </div><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>

            <div className = "row"></div>
            <div className = "row"></div>
            <div className = "row"></div>

            <div className = "row">
            <div className="col-md-0">
            <button type="mainSubmit"  className="btn btn-default" onClick={this.backButton}>Back</button>

            </div>
            <div className="col-md-5">
                
            </div>
            <div className="col-md-6">
                
            </div>
                            
               

            </div><br></br><br></br><br></br><br></br>


            
            </div>
            
        )
    }
}

export default MainPage