import React, { Component } from 'react';

class AddDetails extends Component{

    constructor(props){
        super(props);
 
        this.state = {
            fields: {},
            errors: {}
        }
        this.submitForm = this.submitForm.bind(this);
     }
     
     handleValidation(){
         let fields = this.state.fields;
         let errors = {};
         let formIsValid = true;
 
         //Name
         if(!fields["lname"] || !fields["fname"] || !fields["country"] || !fields["dob"] || !fields["sclname"]
         || !fields["city"]|| !fields["code"]|| !fields["address"]   )
         {
            formIsValid = false;
            errors["lname"] = "Cannot be empty";
            errors["fname"]= "Cannot be empty";
            errors["state"]= "Cannot be empty";
            errors["dob"]= "Cannot be empty";
            errors["password"]= "Cannot be empty";
            errors["city"]= "Cannot be empty";
            errors["phn"]= "Cannot be empty";
            errors["code"]= "Cannot be empty";
            errors["address"]= "Cannot be empty";
         }
        this.setState({errors: errors});
        return formIsValid;
    }
 
    submitForm(e){
         e.preventDefault();
 
         if(this.handleValidation()){
            alert("Form submitted");
         }else{
            alert("Form has errors.")
         }
 
     }
 
     handleChange(field, e){         
         let fields = this.state.fields;
         fields[field] = e.target.value;        
         this.setState({fields});
     }
 
    render(){
        return ( 
        <form>         
        <div class="container-fluid" >
            
            <div class="row"> 
                <div class="col-md-5"></div>
                <div class="col-md-2" >
                <div class="text-primary" >
                    <label for="main"><b><h3>ADD PRODUCT</h3></b></label>
                </div>
                </div>
                <div class="col-md-5"></div>
            </div>
        
        <div class="row">   
            <div className="col-md-3"></div>
                
                <div class="col-md-4">  
                    
                        <div class="form-group">
                        <label for="fname"><b>PRODUCT NAME :</b></label><br></br>
                        <input type="text" ref ="fname"  onChange={this.handleChange.bind(this, "fname")} value={this.state.fields["fname"]} /><br></br>
                        <span style={{color: "red"}}>{this.state.errors["fname"]}</span><br></br>                        

                        <label for="dob"><b>PRODUCT CODE :</b></label><br></br>
                        <input type="text" ref ="dob"  onChange={this.handleChange.bind(this, "dob")} value={this.state.fields["dob"]} /><br></br>
                        <span style={{color: "red"}}>{this.state.errors["dob"]}</span><br></br>

                        <label for="address"><b>EXPIRATION :</b></label><br></br>
                        <input type="text" ref ="address"  onChange={this.handleChange.bind(this, "address")} value={this.state.fields["address"]} /><br></br>
                        <span style={{color: "red"}}>{this.state.errors["address"]}</span><br></br>
                        <br></br>
                        
                        <div class="row">
                            <div class="col-md-2">
                        
                            </div>
                            </div>           
                        </div>
                </div>                                                
        </div>

        <div class="row"> 
                <div class="col-md-3"></div>
                <div class="col-md-4" >
                    <button type="button" class="btn btn-default" id="sumbit" value="Submit" onClick={this.submitForm} >SUBMIT</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-default">VIEW LIST</button>
                </div>
                <div class="col-md-3"></div>
            </div><br></br><br></br><br></br><br></br>
            </div>
            <div className = "row">
            <div className="col-md-1"></div>
            <div className="col-md-3">
                <button type="button" className="btn btn-default" onClick = {() => this.props.history.push(`/main`)} >Back to Main Page</button>
                </div>
            </div><br></br><br></br><br></br><br></br>
        </form> 

            
    
        );
    }
}

export default AddDetails;